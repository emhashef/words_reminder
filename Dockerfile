# pull official base image
FROM python:3.8-alpine3.12

RUN apk update \
    && apk add --virtual build-deps gcc python3-dev musl-dev \
    && apk add --no-cache mariadb-dev 

RUN apk add supervisor \
    && apk add nginx \
    && apk add gettext

RUN mkdir -p /run/nginx
# set work directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN pip install --upgrade pip

COPY requirements.txt ./requirements.txt

RUN pip install -r requirements.txt

RUN pip install gunicorn

COPY nginx.conf /etc/nginx/nginx.conf

COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

COPY . .


CMD ["sh", "./run.sh" ]
