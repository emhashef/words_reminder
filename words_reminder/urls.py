"""words_reminder URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include,reverse_lazy
from django.views.generic import RedirectView,TemplateView
from django.templatetags.static import static
from django.conf.urls.i18n import i18n_patterns

urlpatterns = i18n_patterns(
    path('admin/', admin.site.urls),
    path('', RedirectView.as_view(url=reverse_lazy('dashboard'))),
    path('', include('authentication.urls')),
    path('', include('words.urls')),
    path('dashboard', include('dashboard.urls')),
    path('', include('study.urls')),
    path('dictionaries', include('dictionaries.urls')),
    path('reminders', include('reminders.urls',namespace='reminders')), ## FIXME: remove namespace*
    path('firebase-messaging-sw.js', TemplateView.as_view(template_name='webpush/firebase-messaging-sw.js', content_type='application/javascript')),
    path('favicon.ico', RedirectView.as_view(url=static('favicon.ico'))),
    prefix_default_language = False
)

# FIXME: move prefix of apps urls to here