from django.shortcuts import render
from django.views.generic import TemplateView,View
from rest_framework import views
from rest_framework.response import Response
from dictionaries.backends import DICTIONARIES, get_dictionary
from dictionaries.forms import DictionarySearchForm
from django.contrib.auth.mixins import LoginRequiredMixin
# Create your views here.


# class DictionariesView(LoginRequiredMixin, TemplateView):

#     template_name = 'dictionaries/dictionaries.html'
    
#     def get_context_data(self,**kwargs):
#         kwargs.update(self.get_dictionary_context())
#         return super().get_context_data(**kwargs)
    
#     #TODO: best practice mixins
#     def get_dictionary_context(self,results={}):
#         return {'dictionaries' : [{
#             'name' : d,
#             'label':label,
#             'form':DictionarySearchForm(),
#             'result':results.get(d)} \
#                 for d,label in DICTIONARIES.items()]
#         }
    
# class DictionariesResultView(LoginRequiredMixin, views.APIView):
#     def post(self,request,dictionary_name,*args, **kwargs):
#         q = self.request.POST.get('search')
#         dictionary = get_dictionary(dictionary_name)
#         definition = dictionary.get_definition(q,type='html')
        
#         data = {
#             'dictionary' : dictionary.title,
#             'result' : definition,
#             'word' : q,
#             'direction' : dictionary.direction
#         }
        
#         return Response(data)
    
    