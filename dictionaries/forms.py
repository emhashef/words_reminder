from django.forms import Form,CharField


class DictionarySearchForm(Form):
    search = CharField(max_length=125,required=True)