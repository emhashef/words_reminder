import requests
from bs4 import BeautifulSoup
from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import gettext_lazy as _
#TODO: define mehtod of http for get_url
#TODO: define headers for http request
# FIXME:  reanalyze dictionaries classes for better bestpractices


class BaseDictionary:
    title = None
    slug = None
    direction = 'rtl'
    
    def __init__(self):
        self._word = None
    
    def get_slug(self):
        return self.slug
    
    def get_title(self):
        return self.title
    
    def get_definition(self, word):
        raise NotImplementedError()

class WebDictionary(BaseDictionary):
    
    http_method = 'GET'
    data = None
    url = None
    css_selector = None
    
    def get_site_content(self):
        method = self.get_http_method()
        url = self.get_url()
        data = self.get_data()
        return requests.request(method,url,data=data).text
    
    def get_data(self, **kwargs):
        if self.data is not None:
            kwargs.update(self.data)
        return {key: value.format(**{'word': self._word}) for key,value in kwargs.items()}
    
    def get_http_method(self):
        supported_methods = ['GET','POST']
        method = self.http_method.upper()
        if method not in supported_methods:
            raise Exception('http method not supported!')
        
        return method
    
    def get_url(self):
        if self.url is None:
            raise ImproperlyConfigured('url of web dictionary not setted!')
        
        return self.url.format(**{'word': self._word})
    
    def get_definition(self, word):
        self._word = word
        content = self.get_site_content()
        if content:
            return self.parse_content(content)

        return None
        
    def parse_content(self, content):
        soup = BeautifulSoup(content, 'html.parser')
        return soup.select_one(self.get_css_selector())
    

    def get_css_selector(self):
        return self.css_selector