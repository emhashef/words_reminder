from dictionaries.classes import WebDictionary
from bs4 import BeautifulSoup
import requests

class Oxfoard(WebDictionary):
    title = 'Oxfoard'
    slug = 'oxfoard'
    url = 'https://www.oxfordlearnersdictionaries.com/definition/english/{word}/'
    css_selector = '#entryContent > div > ol'
    
    direction = 'ltr'
    
    
    def parse_content(self, content):
        definition = super().parse_content(content)
        if definition:
            return '<link href="https://www.oxfordlearnersdictionaries.com/' \
                'external/styles/oald10.css?version=2.1.5" rel="stylesheet" ' \
                'type="text/css">\n' + str(definition)
        return definition
         
        
        