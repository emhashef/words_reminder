from dictionaries.classes import WebDictionary

class Abadis(WebDictionary):
    title = 'Abadis'
    slug = 'abadis'
    url = 'http://abadis.ir/api/?cmd=chword&ver=3'
    data = {
        'Word' : '{word}',
        'Fr' : 'auto',
        'To' : 'fa',
    }

    http_method = 'POST'
    def parse_content(self, content):
        items = str(content).split('^')
        for item in items:
            if not item:
                continue
            word = item.split('#')[0]
            mean = item.split('#')[1]
            if word == self._word:
                return f"<p>{mean}</p>"
        return None