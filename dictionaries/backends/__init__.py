import sys
import inspect
from dictionaries.classes import BaseDictionary

DICTIONARIES = [
    'oxfoard',
    'abadis' 
]


# design this like reminders
def inherits_from(child, parent):
    if inspect.isclass(child) and inspect.isclass(parent):
        if str(parent) in [str(c) for c in inspect.getmro(child)[1:]]:
            return True
    return False

def get_dictionary(dictionary):

    path = 'dictionaries.backends.%s' % dictionary
    
    __import__(path)
    module = sys.modules[path]
    attrs = dir(module)
    for attr in attrs:
        obj = getattr(module,attr)
        if inherits_from(obj,BaseDictionary):
            return getattr(module,attr)()


DICTIONARY_TITLES =  [get_dictionary(d).title for d in DICTIONARIES]

DICTIONARY_TYPES = zip(DICTIONARIES, DICTIONARY_TITLES)

