from rest_framework.serializers import ModelSerializer
from rest_framework.fields import ListField
from words.models import Word

class DefinitionsField(ListField):
    pass

class WordSerializer(ModelSerializer):
    class Meta:
        model = Word
        fields = ('value')