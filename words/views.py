from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect
from django.views.decorators.http import require_http_methods
from django.views.generic import FormView,ListView,TemplateView,View,CreateView,UpdateView,DeleteView
from django.views.generic.edit import ModelFormMixin, SingleObjectMixin
from django.forms import modelformset_factory
from words.models import WordList,Word
from words.forms import *
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.
class WordListView(LoginRequiredMixin, ModelFormMixin,ListView):
    
    http_method_names = ['get']
    model = WordList
    
    template_name = 'words/word_list.html'
    fields = ('title','dictionaries')
    def get_context_data(self, **kwargs):
        self.object = None
        kwargs['wordlist_forms'] = self.get_forms()
        kwargs['form'] = self.get_form()
        return super().get_context_data(**kwargs)
    
    def get_queryset(self):
        return self.request.user.wordlists.all()
    
    def get_forms(self):
        forms = list()
        form_class = self.get_form_class()
        for item in self.get_queryset():
            kwargs = self.get_form_kwargs()
            kwargs.update({'instance' : item})
            forms.append(form_class(**kwargs))
        return forms

class CreateWordListView(LoginRequiredMixin, CreateView):
    http_method_names = ['post']
    model = WordList
    fields = ['dictionaries','title']
    success_url = reverse_lazy('wordlists')
    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(CreateWordListView, self).form_valid(form)
    
class UpdateWordListView(LoginRequiredMixin, UpdateView):
    template_name = 'words/word_list.html'
    http_method_names = ['post']
    model = WordList
    fields = ['dictionaries','title']
    success_url = reverse_lazy('wordlists')
    pk_url_kwarg = 'wordlist_id'
    
class DeleteWordListView(LoginRequiredMixin, DeleteView):
    success_url = reverse_lazy('wordlists')
    pk_url_kwarg = 'wordlist_id'
    model = WordList

    
    
###############################################################
class RelationObjectMixin:
    rel_pk_url_kwarg = 'pk'
    
    
    def dispatch(self, *args, **kwargs):
        self.rel_object = self.get_relational_object()
        return super().dispatch(*args, **kwargs)
    
    def get_relational_object(self):
        return get_object_or_404(
            self.get_relational_queryset(), 
            pk=self.kwargs.get(self.rel_pk_url_kwarg)
            )
    
    def get_context_data(self, **kwargs):
        kwargs.update({
            'rel_object' : self.rel_object
        });
        return super().get_context_data(**kwargs)
    


class AddWordView(LoginRequiredMixin, RelationObjectMixin, FormView):
    template_name = 'words/add_word.html'
    
    form_class = AddWordForm
    success_url = reverse_lazy('add_word')
    rel_pk_url_kwarg = 'wordlist_id'
    
    
    def form_valid(self, form):

        values = form.cleaned_data['values']
        word_objects = [Word(value=value, wordlist=self.rel_object) for value in values]
        Word.objects.bulk_create(word_objects)
        messages.success(self.request, 'words added successfuly')   
        return super().form_valid(form)
    
    def get_context_data(self, **kwargs):
        """Insert the forms into the context dict."""
        kwargs.update({
            'filewordform' : FileAddWordForm(),
            })
        return super().get_context_data(**kwargs)

    def get_relational_queryset(self):
        return self.request.user.wordlists
    
    def get_success_url(self):
        return reverse_lazy('add_word',args=[self.rel_object.id])
    
class FileWordAddView(LoginRequiredMixin, RelationObjectMixin, FormView):
    http_method_names = ['post']
    form_class = FileAddWordForm
    rel_pk_url_kwarg = 'wordlist_id'
    
    def form_valid(self, form):
        
        values = form.cleaned_data['file'] 
        word_objects = [Word(value=value, wordlist=self.rel_object) for value in values]
        Word.objects.bulk_create(word_objects)
        
        messages.success(self.request, 'words added successfuly')   
        return super().form_valid(form)

    def get_relational_queryset(self):
        return self.request.user.wordlists.all()

    def get_success_url(self):
        return reverse_lazy('add_word',args=[self.rel_object.id])


class WordsView(LoginRequiredMixin, RelationObjectMixin, ListView):
    template_name = 'words/words_list.html'
    rel_pk_url_kwarg = 'wordlist_id'
    def get_queryset(self):
        return self.rel_object.words.all()
    
    def get_relational_queryset(self):
        return self.request.user.wordlists

class DeleteWordsView(LoginRequiredMixin, RelationObjectMixin, DeleteView):
    rel_pk_url_kwarg = 'wordlist_id'
    pk_url_kwarg = 'word_id'
    def get_queryset(self):
        return self.rel_object.words.all()
    
    def get_relational_queryset(self):
        return self.request.user.wordlists.all()
    
    def get_success_url(self):
        return reverse_lazy('words_list',args=[self.rel_object.id])