from django import forms
from words.models import Word,WordList
# from multiselectfield.forms.fields import MultiSelectFormField
from django.forms import modelformset_factory


class WordValuesField(forms.Field):
    def clean(self, value):
        return [w.strip().lower() for w in value.split('\n') if w.strip()]

class WordValuesFileField(forms.FileField):
    def clean(self, data):
        data = super().clean(data)
        return [w.strip().lower() for w in data if w.strip()]
        
class AddWordForm(forms.Form):
    values = WordValuesField(widget=forms.Textarea())

        
class FileAddWordForm(forms.Form):
    file = forms.FileField()
        


class WordListForm(forms.ModelForm):
    class Meta:
        model = WordList
        fields = ['dictionaries','title']

WordListFormSet = modelformset_factory(WordList,fields=('dictionaries','title'),extra=0)