from django.urls import path,include
from words.views import *
urlpatterns = [
    path('wordlists/',WordListView.as_view(),name='wordlists'),
    path('wordlists/create',CreateWordListView.as_view(),name='create_wordlist'),
    path('wordlists/<int:wordlist_id>/',UpdateWordListView.as_view(),name='update_wordlist'),
    path('wordlists/<int:wordlist_id>/remove', DeleteWordListView.as_view(), name='remove_wordlist'),
    path('wordlists/<int:wordlist_id>/words/',WordsView.as_view(),name='words_list'),
    path('wordlists/<int:wordlist_id>/words/add',AddWordView.as_view(),name='add_word'),
    path('wordlists/<int:wordlist_id>/words/add_file',FileWordAddView.as_view(),name='add_word_from_file'),
    path('wordlists/<int:wordlist_id>/words/<int:word_id>/remove',DeleteWordsView.as_view(),name='remove_word'),
    
]
