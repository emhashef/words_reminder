from django.db import models
from django.utils import timezone
from django.conf import settings
from multiselectfield import MultiSelectField
from dictionaries.backends import DICTIONARY_TYPES,get_dictionary
from django.utils.translation import gettext_lazy as _
from django.db.models import Q

# Create your models here.
class WordList(models.Model):
    title = models.CharField(null=True,max_length=255)
    # words = models.F(Word, through='WordListWord')
    user = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE,related_name='wordlists')
    dictionaries = MultiSelectField(choices=DICTIONARY_TYPES,min_choices=1,default='abadis')
    last_remind = models.DateTimeField(null=True)
    
    def simple_words(self):
        return self.words.all()[:5]
    
    def __str__(self):
        return self.title
    
    
    def on_due_words(self):
        now = timezone.now()
        return self.words \
            .filter(Q(status__exact=Word.Status.NEW) | \
                    Q(status__exact=Word.Status.LEARNING,due_at__lte=now)) \
            .order_by('status','level','added_at')
            
            
class Word(models.Model):
    class Level(models.IntegerChoices):
        ONE = 1
        TWO = 2
        THREE = 3
        FOUR = 4
        FIVE = 5
        
    class Status(models.TextChoices):
        NEW = 'new',_('New')
        LEARNING = 'learning',_('Learning')
        LEARNED = 'learned',_('Learned')
        
    value = models.CharField(max_length=255)
    due_at = models.DateTimeField(null=True)
    wordlist = models.ForeignKey(WordList,on_delete=models.CASCADE,related_name='words')
    added_at = models.DateTimeField(default=timezone.now)
    level = models.IntegerField(choices=Level.choices,default=Level.ONE)
    status = models.CharField(max_length=8,choices=Status.choices,default=Status.NEW)
    
    def __str__(self):
        return self.value
    
    def get_definitions(self):
        # FIXME: this is not best practice, fix it later
        defs = []
        for dictionary in self.wordlist.dictionaries:
            d = get_dictionary(dictionary)
            definition = d.get_definition(self.value)
            defs.append({
                'title' : d.title,
                'definition' : definition,
                'direction' : d.direction,
            });
        return defs
    
    def answer(self, answer):
        if self.level == Word.Level.FIVE:
            self.status = Word.Status.LEARNED
            self.save()
            return self
        
        self.due_at = self.calculate_due_datetime(answer)
        self.level = self.calculate_word_level(answer)
        self.status = Word.Status.LEARNING
        self.save()
        return self
            

    def calculate_due_datetime(self, answer):
        due_at = timezone.now()
        if answer == 2: # word is new for user
            due_at += timezone.timedelta(days=1)
        elif answer == 1: # word is known for user
            if self.level == Word.Level.ONE:
                due_at += timezone.timedelta(days=2)
            elif self.level == Word.Level.TWO:
                due_at += timezone.timedelta(days=4)
            elif self.level == Word.Level.THREE:
                due_at += timezone.timedelta(days=8)
            elif self.level == Word.Level.FOUR:
                due_at += timezone.timedelta(days=16)
        elif answer == 0: # word forgotten by user
            due_at += timezone.timedelta(days=1)
        
        return due_at
    
    def calculate_word_level(self,answer):
        if answer == 2:
            return Word.Level.ONE
        elif answer == 1:
            return self.level + 1
        elif answer == 0:
            return Word.Level.ONE

