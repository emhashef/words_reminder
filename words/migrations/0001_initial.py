# Generated by Django 3.0.7 on 2020-06-17 13:36

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import multiselectfield.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='WordList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, null=True)),
                ('dictionaries', multiselectfield.db.fields.MultiSelectField(choices=[('oxfoard', 'Oxfoard'), ('abadis', 'Abadis')], default='abadis', max_length=14)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Word',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(max_length=255)),
                ('due_at', models.DateTimeField(null=True)),
                ('added_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('wordlist', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='words', to='words.WordList')),
            ],
        ),
    ]
