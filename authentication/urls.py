from django.urls import include,path
from authentication.views import CustomLoginView,RegisterView
urlpatterns = [
     path('register/',RegisterView.as_view(),name='register'),
     path('login/',CustomLoginView.as_view(),name='login'),
     path('', include('django.contrib.auth.urls')),
     
]
