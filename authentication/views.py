
from django.urls import reverse_lazy
from django.contrib.auth.views import LoginView
from django.views.generic import CreateView, View
from authentication.forms import LoginForm
from authentication.forms import CustomUserCreationForm,UserCreationForm


class CustomLoginView(LoginView):
     form_class = LoginForm

     def form_valid(self, form):

        remember_me = form.cleaned_data['remember_me']  # get remember me data from cleaned_data of form
        if not remember_me:
             self.request.session.set_expiry(0)  # if remember me is 
             self.request.session.modified = True
        return super(CustomLoginView, self).form_valid(form)
    
class RegisterView(CreateView):
    template_name = 'registration/register.html'
    form_class =  CustomUserCreationForm
    success_url = reverse_lazy('dashboard')

            
            
            
        
    
    