from django.db import models
from django.contrib.auth.models import AbstractUser
from words.models import Word
from django.db.models import Q
from django.utils import timezone
# Create your models here.

class User(AbstractUser):
    
    def words(self):
        return Word.objects.filter(wordlist__user=self)
    
    def on_due_words(self):
        now = timezone.now()
        return self.words() \
            .filter(Q(status__exact=Word.Status.NEW) | \
                    Q(status__exact=Word.Status.LEARNING,due_at__lte=now)) \
            .order_by('status','level','added_at')


