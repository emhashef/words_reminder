# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-13 15:18+0000\n"
"PO-Revision-Date: 2020-10-06 23:24+0330\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 2.4.1\n"

#: templates/base.html:95
msgid "Profile"
msgstr "پروفایل"

#: templates/base.html:98
msgid "Settings"
msgstr "تنظیمات"

#: templates/base.html:101
msgid "Activity Log"
msgstr "گزارش فعالیت"

#: templates/base.html:105 templates/base.html:264
msgid "Logout"
msgstr "خروج از سیستم"

#: templates/base.html:125
msgid "Dashboard"
msgstr "داشبورد"

#: templates/base.html:152 templates/study/study.html:5
msgid "Study"
msgstr "یادگیری"

#: templates/base.html:183
msgid "Words List"
msgstr "لیست لغات"

#: templates/base.html:191
msgid "Reminders"
msgstr "یادآورها"

#: templates/base.html:256
msgid "Are you sure you want to logout?"
msgstr "آیا مطمئن هستید که میخواهید از سیستم خارج شوید؟"

#: templates/base.html:261
msgid "Click logout to exit"
msgstr "کلیک کنید تا خارج شوید"

#: templates/base.html:263 templates/words/word_list.html:47
msgid "Cancel"
msgstr "لغو"

#: templates/registration/logged_out.html:10
msgid "You are logged out!"
msgstr "شما خارج شده اید!"

#: templates/registration/login.html:14
msgid "Welcome"
msgstr "خوش آمدید"

#: templates/registration/login.html:19
msgid "Enter Email Address"
msgstr "آدرس ایمیل را وارد کنید"

#: templates/registration/login.html:23 templates/registration/register.html:45
msgid "Password"
msgstr "رمز"

#: templates/registration/login.html:29
msgid "Remember me"
msgstr "من را به خاطر بسپار"

#: templates/registration/login.html:32 templates/registration/register.html:70
msgid "Login"
msgstr "وارد شوید"

#: templates/registration/login.html:48 templates/registration/register.html:12
msgid "Create an account"
msgstr "یک حساب کاربری ایجاد کنید"

#: templates/registration/register.html:18
msgid "First Name"
msgstr "نام"

#: templates/registration/register.html:26
msgid "Last Name"
msgstr "نام خانوادگی"

#: templates/registration/register.html:33
msgid "Email Address"
msgstr "آدرس ایمیل"

#: templates/registration/register.html:38
msgid "Username"
msgstr "نام کاربری"

#: templates/registration/register.html:50
msgid "Repeat Password"
msgstr "رمز مجدد"

#: templates/registration/register.html:55
msgid "Register"
msgstr "ثبت نام"

#: templates/registration/register.html:70
msgid "Do you have an account?"
msgstr "آیا حساب کاربری دارید؟"

#: templates/reminders/device_list.html:34
msgid "Device Type"
msgstr "نوع دستگاه"

#: templates/reminders/device_list.html:37
msgid "Last Remind"
msgstr "آخرین یادآوری"

#: templates/reminders/device_list.html:40
msgid "Add Date"
msgstr "زمان افزودن"

#: templates/reminders/device_list.html:45 templates/words/word_list.html:50
msgid "Delete"
msgstr "حذف"

#: templates/reminders/device_list.html:53
msgid "There is no added device"
msgstr "هیچ دستگاهی افزوده نشده است"

#: templates/reminders/device_list.html:78
msgid "This device"
msgstr "این دستگاه"

#: templates/reminders/webpush/add_webpush.html:9
#: templates/words/add_word.html:32
msgid "Add"
msgstr "افزودن"

#: templates/reminders/webpush/add_webpush.html:14
msgid "Add this device"
msgstr "افزودن این دستگاه"

#: templates/study/select_wordlist.html:5
msgid "Select Words List"
msgstr "انتخاب لیست لغات"

#: templates/study/select_wordlist.html:12
msgid "There is not any wordlist for study"
msgstr "لیست لغتی  برای یادگیری وجود ندارد"

#: templates/study/study.html:14
msgid "Start"
msgstr "شروع"

#: templates/study/study.html:29
msgid "Answer"
msgstr "جواب"

#: templates/study/study.html:39
msgid "It was new"
msgstr "جدید بود"

#: templates/study/study.html:40
msgid "It was known"
msgstr "بلد بودم"

#: templates/study/study.html:41
msgid "It was forgotten"
msgstr "یادم رفته بود"

#: templates/study/study.html:88
msgid "There is not any word for learning"
msgstr "لغتی برای یادگیری وجود ندارد"

#: templates/study/study.html:91
msgid "There is a problem in fetching the words"
msgstr "مشکلی در دریافت لغات وجود دارد"

#: templates/words/add_word.html:5 templates/words/add_word.html:10
msgid "Add words"
msgstr "افزودن لغات"

#: templates/words/add_word.html:24
msgid "Word"
msgstr "لغت"

#: templates/words/add_word.html:31
msgid "Seprate the words by enter"
msgstr "لغات را با اینتر از هم جدا کنید"

#: templates/words/add_word.html:38
msgid "From file"
msgstr "از طریق فایل"

#: templates/words/add_word.html:43
msgid "Choose file"
msgstr "فایل را انتخاب کنید"

#: templates/words/add_word.html:48
msgid "Choose text file"
msgstr "فایل text انتخاب کنید"

#: templates/words/add_word.html:50
msgid "Send"
msgstr "ارسال"

#: templates/words/word_list.html:18 templates/words/word_list.html:78
msgid "Title"
msgstr "عنوان"

#: templates/words/word_list.html:25 templates/words/word_list.html:82
msgid "Dictionaries"
msgstr "دیکشنری ها"

#: templates/words/word_list.html:37
#, fuzzy
#| msgid "Words List"
msgid "Words count"
msgstr "لیست لغات"

#: templates/words/word_list.html:46
msgid "Apply"
msgstr "ثبت"

#: templates/words/word_list.html:49
msgid "Update"
msgstr "تغییر"

#: templates/words/word_list.html:51 templates/words/words_list.html:5
msgid "Words"
msgstr "لغات"

#: templates/words/word_list.html:59
msgid "Word examples"
msgstr "نمونه لغات"

#: templates/words/word_list.html:71
msgid "Add Words List"
msgstr "افزودن لیست لغات"

#: templates/words/word_list.html:88
msgid "Create"
msgstr "ایجاد"

#: templates/words/words_list.html:10
msgid "Add word"
msgstr "افزودن لغت"

#: words/models.py:41
msgid "New"
msgstr "جدید"

#: words/models.py:42
msgid "Learning"
msgstr "در حال یادگیری"

#: words/models.py:43
msgid "Learned"
msgstr "یاد گرفته شده"

#: words_reminder/settings.py:177
msgid "Persian"
msgstr "فارسی"

#: words_reminder/settings.py:178
msgid "English"
msgstr "انگلیسی"
