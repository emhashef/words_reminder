from reminders.views import channel_view,DeviceListView,DeviceDeleteView
from django.urls import path, include
from importlib import import_module
from reminders.channels import REMINDER_CHANNELS

app_name = 'reminders'



# Provider urlpatterns, as separate attribute (for reusability).
channel_urlpatterns = []
for _, channel in REMINDER_CHANNELS.items():
    try:
        ch_mod = import_module(channel.get_package() + '.urls')
    except ImportError:
        continue
    ch_urlpatterns = getattr(ch_mod, 'urlpatterns', None)
    if ch_urlpatterns:
        channel_urlpatterns += ch_urlpatterns



urlpatterns = [
    path('/channels/<str:channel_name>', channel_view, name='channel'),
    path('/channels/<str:channel_name>', include(channel_urlpatterns)),
    path("<int:pk>/remove", DeviceDeleteView.as_view(), name="remove"),
    path('', DeviceListView.as_view(), name='list')
]