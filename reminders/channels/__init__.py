from django.conf import settings
from reminders.utils import load_path_attr
DEFAULT_REMINDER_CHANNELS_LIST = [
    'reminders.channels.webpush.reminder.WebPush'
]

REMINDER_CHANNELS_LIST = getattr(settings, 'REMINDER_CHANNELS_LIST', DEFAULT_REMINDER_CHANNELS_LIST)

def get_reminder_channels():
    
    channels = {}
    for channel in REMINDER_CHANNELS_LIST:
        channel_obj = load_path_attr(channel)()
        channels[channel_obj.name] = channel_obj
        
    return channels

REMINDER_CHANNELS = get_reminder_channels()

