from django.views.generic import TemplateView
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from reminders.models import Device
from reminders.channels.webpush.reminder import OneSignalForm
from django.http import HttpResponseBadRequest
import json

class WebPushView(TemplateView):
    template_name = 'reminders/webpush/add_webpush.html'
    
@login_required
def add_webpush_device(request, channel_name):
    form = OneSignalForm(request.POST)
    if form.is_valid():
        device = Device.objects.create(user=request.user, value=json.dumps(form.cleaned_data), channel_type=channel_name)
        return render(request,'reminders/webpush/add_complete.html',{'device': device })
    return HttpResponseBadRequest()
        

channel_view = WebPushView.as_view()