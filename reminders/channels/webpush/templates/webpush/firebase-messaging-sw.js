importScripts('https://www.gstatic.com/firebasejs/7.18.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.18.0/firebase-messaging.js');

var firebaseConfig = {
    apiKey: "AIzaSyDLWnctTiRPPWj6aNJHhrlw0yzJibPBp_E",
    authDomain: "words-reminder-d0378.firebaseapp.com",
    databaseURL: "https://words-reminder-d0378.firebaseio.com",
    projectId: "words-reminder-d0378",
    storageBucket: "words-reminder-d0378.appspot.com",
    messagingSenderId: "1028753352796",
    appId: "1:1028753352796:web:befb2ef93a76c755b05e99",
    measurementId: "G-FFXH6JEWKR"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    const notificationTitle = 'New Notification';
    const notificationOptions = {
        body: 'You Have A New Notification',
        // icon: '/firebase-logo.png'
    };

    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});
self.addEventListener('notificationclick', function(event) {
    if (event.action === 'study') {
        var study_url = event.notification.data.FCM_MSG.notification.data.study_url;
        event.waitUntil(async function(event) {
            const allClients = await clients.matchAll({
                includeUncontrolled: true
            });

            let studyClient;

            // Let's see if we already have a chat window open:
            for (const client of allClients) {
                const url = new URL(client.url);

                if (url.pathname == study_url) {
                    // Excellent, let's use it!
                    client.focus();
                    studyClient = client;
                    break;
                }
            }

            // If we didn't find an existing chat window,
            // open a new one:
            if (!studyClient) {
                studyClient = await clients.openWindow(study_url);
            }

            // Message the client:
            studyClient.postMessage("New chat messages!");
        }());
    }
    event.notification.close();
});