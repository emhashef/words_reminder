#TODO: change name of module to channel.py
from reminders.channels.base import BaseReminder
from onesignal_sdk.client import Client
from onesignal_sdk.error import OneSignalHTTPError
from django.conf import settings
from django import forms
from django.urls import reverse, reverse_lazy
from django.template.loader import render_to_string

import json
import requests

from oauth2client.service_account import ServiceAccountCredentials


SCOPES = ['https://www.googleapis.com/auth/firebase.messaging']

def _get_access_token():
    """Retrieve a valid access token that can be used to authorize requests.
    :return: Access token.
    """
    
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        settings.FCM_AUTH_FILE_PATH, SCOPES)
    access_token_info = credentials.get_access_token()
    return access_token_info.access_token


class WebPush(BaseReminder):
    name = 'webpush'

    title = 'Web Push'

    def remind(self, value, **kwargs):
        """Deliver from webpush channel
        args:
            title: title of notification
            body: body of notification
            icon: icon of webpush notification
            data: data of webpush notification
            actions: actions of webpush notification
        """
        assert 'token' in value, "FCM token must be saved in value"
        fcm_message = {
            'message': {
                'token': value['token'],
                'notification': {
                    'title': kwargs.get('title', settings.SITE_NAME),
                    'body': kwargs.get('body', 'You Have a New Notification'),

                },
                "webpush": {
                    "headers": {
                        "Urgency": "high"
                    },
                    "notification": {
                        "requireInteraction": True,
                        "icon": kwargs.get('icon' ,settings.SITE_ROOT + '/favicon.ico'),
                        "data": kwargs.get('data' ,{}),
                        "actions": kwargs.get('actions' ,[]),


                    }

                }
            }
        }

        headers = {
            'Authorization': 'Bearer ' + _get_access_token(),
            'Content-Type': 'application/json; UTF-8',
        }
        
        FCM_URL = 'https://fcm.googleapis.com/v1/projects/' + \
        settings.FCM_PROJECT_ID + '/messages:send'


        resp = requests.post(FCM_URL, data=json.dumps(
            fcm_message), headers=headers)

        if not resp.status_code == 200:
            raise WebPushHTTPError(resp)

        return resp.text

    def get_values_form_class(self):
        return OneSignalForm

    def get_icon_path(self):
        return 'img/webpush.png'


class OneSignalForm(forms.Form):
    token = forms.CharField(required=True)


class WebPushHTTPError(Exception):
    def __init__(self, response):
        self.http_response = response
        self.message = self._get_message(response)
        self.status_code = response.status_code
        super().__init__(self.message)

    def _get_message(self, response):
        message = f'Unexpected http status code {response.status_code}.'
        response_body = response.json()
        if response_body and 'error' in response_body and 'message' in response_body['error']:
            message = response_body['error']['message']
        return message
