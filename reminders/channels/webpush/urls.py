from django.urls import path
from reminders.channels.webpush.views import add_webpush_device

urlpatterns = [
    path('/',add_webpush_device,name='add_webpush_device')
]
