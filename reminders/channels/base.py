

class BaseReminder:#TODO: change name of class to ChannelBase
    name = None
    title = None
    def remind(self, device, text):#TODO: change name of method to delivery
        raise NotImplementedError()
    
    def get_values_form_class(self):
        raise NotImplementedError()
    
    def get_redirect_url(self):
        return ''
    
    @classmethod
    def get_package(cls):
        pkg = getattr(cls, 'package', None)
        if not pkg:
            pkg = cls.__module__.rpartition('.')[0]
        return pkg