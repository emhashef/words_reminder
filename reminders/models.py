from django.db import models
from authentication.models import User
from reminders.channels import REMINDER_CHANNELS
from reminders.reminders import Reminder
import json
from django.utils import timezone


REMINDER_CHANNELS_CHOICES = [(channel.name, channel.title)
                             for channel in REMINDER_CHANNELS.values()]

# Create your models here.
class Device(models.Model):

    user = models.ForeignKey(User, related_name='devices', on_delete=models.CASCADE)
    value = models.TextField(blank=True)
    channel_type = models.CharField(max_length=255, blank=True, choices=REMINDER_CHANNELS_CHOICES)
    last_remind = models.DateTimeField(null=True)
    added_at = models.DateTimeField(default=timezone.now)
    
    def value_json(self):
        return json.loads(self.value)
    
    def remind(self, reminder: Reminder):
        
        kwargs = getattr(reminder, 'to_' + self.channel_type)(user=self.user)

        r = self.channel.remind(self.value_json(), **kwargs)
        
        self.last_remind = timezone.now()
        self.save()
        
        return r
    
    @property
    def channel(self):
        if self.channel_type in REMINDER_CHANNELS:
            return REMINDER_CHANNELS[self.channel_type]
        # FIXME: fix this exception type
        raise AttributeError(f"'{self.channel_type}' channel not registered in reminder channels list")
        
        
            