from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from reminders.forms import AddDeviceForm
from reminders.channels import REMINDER_CHANNELS
from reminders.models import Device
from django.views.generic import ListView,DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.http import HttpResponseBadRequest, Http404, HttpResponseNotFound,JsonResponse
from importlib import import_module
from django.urls import reverse_lazy



@login_required
@require_http_methods(['GET'])
def channel_view(request, channel_name):
    if channel_name in REMINDER_CHANNELS:
        channel = REMINDER_CHANNELS[channel_name]
        try:
            module = import_module(channel.get_package() + '.views')
            view = getattr(module,'channel_view')
            if view:
                return view(request)
            
        except ImportError:
            return HttpResponseBadRequest('there is not veiw for this channel')
        
        
class DeviceListView(LoginRequiredMixin, ListView):
    # queryset = Device.objects.all()
    def get_queryset(self):
        return Device.objects.filter(user=self.request.user)
    
    def get_context_data(self, **kwargs):
        kwargs.update({
            'channels' : REMINDER_CHANNELS.values()
        })
        return super().get_context_data(**kwargs)
    

class DeviceDeleteView(LoginRequiredMixin, DeleteView):
    success_url = reverse_lazy('reminders:list')
    def get_queryset(self):
        return Device.objects.filter(user=self.request.user)
    