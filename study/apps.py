from django.apps import AppConfig
from django.conf import settings
from django.db import IntegrityError, ProgrammingError, OperationalError
class StudyConfig(AppConfig):
    name = 'study'

    def ready(self):
        from django_q.tasks import IntegrityError
        from django_q.tasks import schedule
        try:
            schedule(
                func='study.tasks.remind',
                name='study_reminder',
                schedule_type='I',
                minutes=settings.REMINDING_BEAT_SCHEDULE / 60,
                repeats=-1   
            )
        except (ProgrammingError, IntegrityError, OperationalError):
            pass