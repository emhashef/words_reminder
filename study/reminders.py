from reminders.reminders import Reminder
from django.urls import reverse_lazy
from django.utils import timezone

class OndueWords(Reminder):
    def __init__(self, wordlist):
        self.wordlist = wordlist

    def to_webpush(self, user):
        return {
            'body' : 'You Have One Due Words In "%s" List, Study Now!' % self.wordlist.title,
            'data' : {'study_url' : '/study/'+str(self.wordlist.id)},
            'actions' : [
                {'title' : 'Study', 'action' : 'study'}
            ],
        }