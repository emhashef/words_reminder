from django.http import Http404
from rest_framework import generics
from study.serializers import AnswerSerializer,OneDueWordSerializer
# Create your views here.

class LastOnDueWordMixin:
    def get_object(self):
        wordlist = self.request.user.wordlists.get(id=self.kwargs['wordlist_id'])
        word = wordlist.on_due_words().last()
        if word is None:
            raise Http404('there is not any on due word !')
        return word

class AnswerWordView(LastOnDueWordMixin, generics.UpdateAPIView):
    serializer_class = AnswerSerializer
    
    def post(self,request,*args, **kwargs):
        return self.put(request, *args, **kwargs)
    

class OnDueWordView(LastOnDueWordMixin, generics.RetrieveAPIView):
    serializer_class = OneDueWordSerializer