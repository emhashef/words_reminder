from django.shortcuts import render
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin


class StudyView(LoginRequiredMixin, TemplateView):
    template_name = 'study/study.html'
    
    def get_context_data(self, **kwargs):
        kwargs.update({'wordlist_id':self.kwargs['wordlist_id']})
        return super().get_context_data(**kwargs)
          
    
class SelectWordlistView(LoginRequiredMixin, TemplateView):
    template_name = 'study/select_wordlist.html'
    
    def get_context_data(self, **kwargs):
        kwargs.update({'wordlists': self.request.user.wordlists.all()})
        return super().get_context_data(**kwargs)
    

        