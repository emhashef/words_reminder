from django.urls import path,include
from study.views import StudyView, SelectWordlistView
from study.api_views import AnswerWordView, OnDueWordView
urlpatterns = [
    path('study/select', SelectWordlistView.as_view(), name='select_wordlist'),
    path('study/<int:wordlist_id>',StudyView.as_view(),name='study'),
    path('api/<int:wordlist_id>/ondue/answer',AnswerWordView.as_view(),name='answer_ondue'),
    path('api/<int:wordlist_id>/ondue',OnDueWordView.as_view(),name='get_ondue'),
]
