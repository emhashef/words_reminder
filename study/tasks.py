
from authentication.models import User
from django.conf import settings
from django.urls import reverse_lazy
from study.reminders import OndueWords
from django.utils.timezone import now, timedelta

from logging import getLogger

logger = getLogger(__name__)

def remind():
    users = User.objects.all()
    for user in users:
        for wordlist in user.wordlists.all():
            logger.info('"%s" ondue count: %d' % (wordlist.title, wordlist.on_due_words().count()))
            if wordlist.on_due_words().count() >= settings.REMINDING_ONDUE_WORDS_THRESHOLD \
                and (not wordlist.last_remind \
                    or now() - wordlist.last_remind >= timedelta(seconds=settings.REMINDING_WORDLIST_THRESHOLD)):
                    
                for device in user.devices.all():
                    try:
                        device.remind(OndueWords(wordlist))
                        wordlist.last_remind = now()
                        wordlist.save()
                        logger.info('user "%s" devices reminded' % user.username)
                    except Exception as error:
                        logger.exception(error)
        