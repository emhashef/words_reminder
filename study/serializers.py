from rest_framework import serializers as sz
from words.models import Word

class StudySerializer(sz.Serializer):
    pass

class AnswerSerializer(sz.Serializer):
    answer = sz.IntegerField(required=True,write_only=True,max_value=2,min_value=0)
    
    def update(self, instance, validated_data):
        instance.answer(validated_data.get('answer'))
        return self.instance
        
    
class OneDueWordSerializer(sz.Serializer):
    word = sz.CharField(source='value', read_only=True)
    definitions = sz.ListField(source='get_definitions', read_only=True) 
    status = sz.CharField(read_only=True)
    