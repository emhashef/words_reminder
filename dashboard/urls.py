from django.urls import path
from dashboard.views import DashboardView
from django.contrib.auth.decorators import login_required
urlpatterns = [
    path('',DashboardView.as_view(),name="dashboard")
]
