from django.conf import settings


def branding(request):
    return {"SITE_NAME": settings.SITE_NAME, "SITE_ROOT": settings.SITE_ROOT}